import sqlite3
import time
from time import time as timer

import psutil

interval = 60

while True:
    with open('load_cpu_memory.db', 'r') as f:
        cpu_in = psutil.cpu_percent(interval=1, percpu=True)
        mem_in = psutil.virtual_memory()
        print(cpu_in, mem_in)

        conn = sqlite3.connect('load_cpu_memory.db')
        cursor = conn.cursor()
        cursor.execute('''INSERT INTO load_cpu_memory(cpu, mem) VALUES (cpu_in, mem_in)''')
        conn.commit()
        conn.close()

        time.sleep(interval - timer() % interval)
